$(document).on('ready', function() {
    $(".slider").slick({
        dots: false,
        prevArrow: $('.prev'),
        nextArrow: $('.next')
    });
});


google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([
        ['Year', '1', { role: 'annotation' }, '2', { role: 'annotation' }, '3', { role: 'annotation' } ],
        ['2016', 10, '10%', 60, '60%', 30, '30%'],
        ['2017', 27, '27%', 55, '55%', 18, '18%'],
        ['2018', 25, '25%', 50, '50%', 25, '25%']
    ]);

    var options_stacked = {
        chartArea: {
            width: '35%'
        },
        isStacked: 'percent',
        height: 500,
        width: '100%',
        legend: 'none',
        backgroundColor: '#F5F5F5',
        vAxis: {
            minValue: 0,
            ticks: [
                {v: 0, f: '0'},
                {v: 0.1, f: ''},
                {v: 0.2, f: ''},
                {v: 0.3, f: ''},
                {v: 0.4, f: ''},
                {v: 0.5, f: '50%'},
                {v: 0.6, f: ''},
                {v: 0.7, f: ''},
                {v: 0.8, f: ''},
                {v: 0.9, f: ''},
                {v: 1, f: '100%'},
            ],
            format: 'percent',
            textStyle: {
                color: 'black',
                fontName: 'HelveticaRegular',
                fontSize: 12,
                bold: false,
            }
        },
        hAxis: {
            textStyle: {
                color: 'black',
                fontName: 'HelveticaRegular',
                fontSize: 14,
                bold: true,
            }
        },
        annotations: {
            textStyle: {
                fontName: 'HelveticaRegular',
                fontSize: 12,
                color: '#000000'
            }
        },
        colors: ['#2B4989', '#405E9E', '#617FBD']

    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(data, options_stacked);
}

google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

    var data = google.visualization.arrayToDataTable([
        ['Number', 'Significance', { role: 'style' }, { role: 'annotation' } ],
        ['1', 24.2, 'color: #2B4989', '24.2' ],
        ['2', 13.4, 'color: #2B4989', '13.4' ],
        ['3', 10.4, 'color: #2B4989', '10.4' ],
        ['4', 4.6, 'color: #2B4989', '4.6' ],
        ['5', 4.1, 'color: #2B4989', '4.1' ],
        ['6', 4.0, 'color: #2B4989', '4.0' ],
        ['7', 3.3, 'color: #2B4989', '3.3' ],
        ['8', 2.1, 'color: #D2D2D2', '2.1' ],
    ]);

    var options = {
        backgroundColor: '#F5F5F5',
        chartArea: {
            width: '90%'
        },
        legend: 'none',
        height: 500,
        width: '100%',
        vAxis: {
            baselineColor: '#F5F5F5',
            textPosition: 'out'
        },
        hAxis: {
            gridlines: {
                color: '#F5F5F5'
            },
            baselineColor: '#F5F5F5',
            textPosition: 'none',

        },
        vAxis: {
            textStyle: {
                color: 'black',
                fontName: 'HelveticaRegular',
                fontSize: 16
            }
        },
        annotations: {
            alwaysOutside : true,
            datum: {
                stem: {
                    color: 'none',
                    length: 10
                }
            },
            textStyle: {
                fontName: 'HelveticaRegular',
                fontSize: 14,
                color: '#000000',
                auraColor: 'none'
            }
        }
    };

    var chart = new google.visualization.BarChart(document.getElementById('chart_div-2'));

    chart.draw(data, options);
}


google.charts.setOnLoadCallback(drawBasic2);

function drawBasic2() {

    var data = google.visualization.arrayToDataTable([
        ['Number', 'Significance', { role: 'style' }, { role: 'annotation' } ],
        ['1', 1.5, 'color: #2B4989', '1.5' ],
        ['2', -0.5, 'color: #2B4989', '-0.5' ],
        ['3', 0.2, 'color: #2B4989', '0.2' ],
        ['4', -0.7, 'color: #2B4989', '-0.7' ],
        ['5', 0, 'color: #2B4989', '0' ],
        ['6', 0.9, 'color: #2B4989', '0.9' ],
        ['7', 1.2, 'color: #2B4989', '1.2' ],
        ['8', -0.2, 'color: #D2D2D2', '2.1' ],
    ]);

    var options = {
        backgroundColor: '#F5F5F5',
        chartArea: {
            width: '70%'
        },
        legend: 'none',
        height: 500,
        width: '100%',
        vAxis: {
            baselineColor: '#F5F5F5',
            textPosition: 'none'
        },
        hAxis: {
            gridlines: {
                color: '#F5F5F5'
            },
            textPosition: 'none'
        },
        annotations: {
            alwaysOutside : true,
            datum: {
                stem: {
                    color: 'none',
                    length: 10
                }
            },
            textStyle: {
                fontName: 'HelveticaRegular',
                fontSize: 14,
                color: '#000000',
                auraColor: 'none'
            }
        }
    };

    var chart = new google.visualization.BarChart(document.getElementById('chart_div-3'));

    chart.draw(data, options);
}
