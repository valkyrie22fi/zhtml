var smartgrid = require('smart-grid');

var settings = {
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '20px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    oldSizeStyle: false,
    container: {
        maxWidth: '1280px', /* max-width оn very large screen */
        fields: '100px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1280px' /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '768px',
            fields: '35px' /* set fields only if you want to change container.fields */
        },
        sm: {
            width: '640px',
            fields: '30px' /* set fields only if you want to change container.fields */
        }
    }
};

smartgrid('./src/scss', settings);